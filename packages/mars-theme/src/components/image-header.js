import React, { useEffect } from "react";
import { connect, styled } from "frontity";

const ImageHeader = ({ state, actions, libraries }) => {
  const Html2React = libraries.html2react.Component;
  const med = state.theme.media;

  useEffect(() => {
    if (med.length == 0) {
      const getData = async () => {
        await libraries.source.api
          .get({
            endpoint: "media"
          })
          .then(res => {
            res.json().then(data => {
              state.theme.media.push(data);
            });
          });
      };
      getData();
    }
  }, [med]);

  return med && med[0] && <Html2React html={med[0][1].description.rendered} />;
};

export default connect(ImageHeader);
