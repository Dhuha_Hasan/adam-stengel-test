import React from "react";
import { connect, styled } from "frontity";

const Footer = ({ state, actions, libraries }) => {
  const getCurrentYear = new Date().getFullYear().toString();

  return <FooterText>Copyright © {getCurrentYear} adamstengel.com</FooterText>;
};

export default connect(Footer);

const FooterText = styled.h4`
  margin: 3px;
`;
