const settings = {
  name: "adam-stengel",
  state: {
    frontity: {
      url: "https://test.frontity.io",
      title: "adamstengel.com",
      description: "WORDS AND MUSIC "
    }
  },
  packages: [
    {
      name: "@frontity/mars-theme",
      state: {
        theme: {
          menu: [
            ["Home", "/"],
            ["About us", "/about/"],
            ["Services", "/services/"],
            ["Blog", "/blog/"],
            ["Contact Us", "/contact/"],
            ["FAQ", "/faq/"],
            ["Portfolio", "/portfolio/"],
            ["Gallery", "/gallery/"]
          ],
          featured: {
            showOnList: false,
            showOnPost: false
          },
          media: []
        }
      }
    },
    {
      name: "@frontity/wp-source",
      state: {
        source: {
          api: "http://adamstengel.com/wp-json"
        }
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react"
  ]
};

export default settings;
